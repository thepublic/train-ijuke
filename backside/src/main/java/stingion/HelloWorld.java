package stingion;

/**
 * Created by ievgen on 7/31/14 8:58 PM.
 */
public class HelloWorld {
    public static String getHelloWorld() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        System.out.println(getHelloWorld());
    }
}
