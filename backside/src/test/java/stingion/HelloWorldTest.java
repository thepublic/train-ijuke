package stingion;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ievgen on 7/31/14 8:59 PM.
 */
public class HelloWorldTest {

    @Test
    public void testGetHelloWorld() throws Exception {
        assertEquals("Hello World!", HelloWorld.getHelloWorld());
    }
}
